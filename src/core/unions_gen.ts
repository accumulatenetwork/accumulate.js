import * as types from ".";

// DO NOT EDIT. Generated by gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types.

/* eslint-disable @typescript-eslint/no-namespace */

export type Account =
  | types.ADI
  | types.AnchorLedger
  | types.BlockLedger
  | types.DataAccount
  | types.KeyBook
  | types.KeyPage
  | types.LiteDataAccount
  | types.LiteIdentity
  | types.LiteTokenAccount
  | types.SyntheticLedger
  | types.SystemLedger
  | types.TokenAccount
  | types.TokenIssuer
  | types.UnknownAccount
  | types.UnknownSigner;

export type AccountArgs =
  | types.ADI
  | types.ADIArgsWithType
  | types.AnchorLedger
  | types.AnchorLedgerArgsWithType
  | types.BlockLedger
  | types.BlockLedgerArgsWithType
  | types.DataAccount
  | types.DataAccountArgsWithType
  | types.KeyBook
  | types.KeyBookArgsWithType
  | types.KeyPage
  | types.KeyPageArgsWithType
  | types.LiteDataAccount
  | types.LiteDataAccountArgsWithType
  | types.LiteIdentity
  | types.LiteIdentityArgsWithType
  | types.LiteTokenAccount
  | types.LiteTokenAccountArgsWithType
  | types.SyntheticLedger
  | types.SyntheticLedgerArgsWithType
  | types.SystemLedger
  | types.SystemLedgerArgsWithType
  | types.TokenAccount
  | types.TokenAccountArgsWithType
  | types.TokenIssuer
  | types.TokenIssuerArgsWithType
  | types.UnknownAccount
  | types.UnknownAccountArgsWithType
  | types.UnknownSigner
  | types.UnknownSignerArgsWithType;

/** @ignore */
export namespace Account {
  export function fromObject(obj: AccountArgs): Account {
    if (obj instanceof types.ADI) return obj;
    if (obj instanceof types.AnchorLedger) return obj;
    if (obj instanceof types.BlockLedger) return obj;
    if (obj instanceof types.DataAccount) return obj;
    if (obj instanceof types.KeyBook) return obj;
    if (obj instanceof types.KeyPage) return obj;
    if (obj instanceof types.LiteDataAccount) return obj;
    if (obj instanceof types.LiteIdentity) return obj;
    if (obj instanceof types.LiteTokenAccount) return obj;
    if (obj instanceof types.SyntheticLedger) return obj;
    if (obj instanceof types.SystemLedger) return obj;
    if (obj instanceof types.TokenAccount) return obj;
    if (obj instanceof types.TokenIssuer) return obj;
    if (obj instanceof types.UnknownAccount) return obj;
    if (obj instanceof types.UnknownSigner) return obj;

    switch (obj.type) {
      case types.AccountType.Identity:
      case "identity":
        return new types.ADI(obj);
      case types.AccountType.AnchorLedger:
      case "anchorLedger":
        return new types.AnchorLedger(obj);
      case types.AccountType.BlockLedger:
      case "blockLedger":
        return new types.BlockLedger(obj);
      case types.AccountType.DataAccount:
      case "dataAccount":
        return new types.DataAccount(obj);
      case types.AccountType.KeyBook:
      case "keyBook":
        return new types.KeyBook(obj);
      case types.AccountType.KeyPage:
      case "keyPage":
        return new types.KeyPage(obj);
      case types.AccountType.LiteDataAccount:
      case "liteDataAccount":
        return new types.LiteDataAccount(obj);
      case types.AccountType.LiteIdentity:
      case "liteIdentity":
        return new types.LiteIdentity(obj);
      case types.AccountType.LiteTokenAccount:
      case "liteTokenAccount":
        return new types.LiteTokenAccount(obj);
      case types.AccountType.SyntheticLedger:
      case "syntheticLedger":
        return new types.SyntheticLedger(obj);
      case types.AccountType.SystemLedger:
      case "systemLedger":
        return new types.SystemLedger(obj);
      case types.AccountType.TokenAccount:
      case "tokenAccount":
        return new types.TokenAccount(obj);
      case types.AccountType.TokenIssuer:
      case "tokenIssuer":
        return new types.TokenIssuer(obj);
      case types.AccountType.Unknown:
      case "unknown":
        return new types.UnknownAccount(obj);
      case types.AccountType.UnknownSigner:
      case "unknownSigner":
        return new types.UnknownSigner(obj);
      default:
        throw new Error(`Unknown account '${(obj as any).type}'`);
    }
  }
}

export type DataEntry =
  | types.AccumulateDataEntry
  | types.DoubleHashDataEntry
  | types.FactomDataEntryWrapper;

export type DataEntryArgs =
  | types.AccumulateDataEntry
  | types.AccumulateDataEntryArgsWithType
  | types.DoubleHashDataEntry
  | types.DoubleHashDataEntryArgsWithType
  | types.FactomDataEntryWrapper
  | types.FactomDataEntryWrapperArgsWithType;

/** @ignore */
export namespace DataEntry {
  export function fromObject(obj: DataEntryArgs): DataEntry {
    if (obj instanceof types.AccumulateDataEntry) return obj;
    if (obj instanceof types.DoubleHashDataEntry) return obj;
    if (obj instanceof types.FactomDataEntryWrapper) return obj;

    switch (obj.type) {
      case types.DataEntryType.Accumulate:
      case "accumulate":
        return new types.AccumulateDataEntry(obj);
      case types.DataEntryType.DoubleHash:
      case "doubleHash":
        return new types.DoubleHashDataEntry(obj);
      case types.DataEntryType.Factom:
      case "factom":
        return new types.FactomDataEntryWrapper(obj);
      default:
        throw new Error(`Unknown data entry '${(obj as any).type}'`);
    }
  }
}

export type TransactionBody =
  | types.AcmeFaucet
  | types.ActivateProtocolVersion
  | types.AddCredits
  | types.BlockValidatorAnchor
  | types.BurnCredits
  | types.BurnTokens
  | types.CreateDataAccount
  | types.CreateIdentity
  | types.CreateKeyBook
  | types.CreateKeyPage
  | types.CreateLiteTokenAccount
  | types.CreateToken
  | types.CreateTokenAccount
  | types.DirectoryAnchor
  | types.IssueTokens
  | types.LockAccount
  | types.NetworkMaintenance
  | types.RemoteTransaction
  | types.SendTokens
  | types.SyntheticBurnTokens
  | types.SyntheticCreateIdentity
  | types.SyntheticDepositCredits
  | types.SyntheticDepositTokens
  | types.SyntheticForwardTransaction
  | types.SyntheticWriteData
  | types.SystemGenesis
  | types.SystemWriteData
  | types.TransferCredits
  | types.UpdateAccountAuth
  | types.UpdateKey
  | types.UpdateKeyPage
  | types.WriteData
  | types.WriteDataTo;

export type TransactionBodyArgs =
  | types.AcmeFaucet
  | types.AcmeFaucetArgsWithType
  | types.ActivateProtocolVersion
  | types.ActivateProtocolVersionArgsWithType
  | types.AddCredits
  | types.AddCreditsArgsWithType
  | types.BlockValidatorAnchor
  | types.BlockValidatorAnchorArgsWithType
  | types.BurnCredits
  | types.BurnCreditsArgsWithType
  | types.BurnTokens
  | types.BurnTokensArgsWithType
  | types.CreateDataAccount
  | types.CreateDataAccountArgsWithType
  | types.CreateIdentity
  | types.CreateIdentityArgsWithType
  | types.CreateKeyBook
  | types.CreateKeyBookArgsWithType
  | types.CreateKeyPage
  | types.CreateKeyPageArgsWithType
  | types.CreateLiteTokenAccount
  | types.CreateLiteTokenAccountArgsWithType
  | types.CreateToken
  | types.CreateTokenArgsWithType
  | types.CreateTokenAccount
  | types.CreateTokenAccountArgsWithType
  | types.DirectoryAnchor
  | types.DirectoryAnchorArgsWithType
  | types.IssueTokens
  | types.IssueTokensArgsWithType
  | types.LockAccount
  | types.LockAccountArgsWithType
  | types.NetworkMaintenance
  | types.NetworkMaintenanceArgsWithType
  | types.RemoteTransaction
  | types.RemoteTransactionArgsWithType
  | types.SendTokens
  | types.SendTokensArgsWithType
  | types.SyntheticBurnTokens
  | types.SyntheticBurnTokensArgsWithType
  | types.SyntheticCreateIdentity
  | types.SyntheticCreateIdentityArgsWithType
  | types.SyntheticDepositCredits
  | types.SyntheticDepositCreditsArgsWithType
  | types.SyntheticDepositTokens
  | types.SyntheticDepositTokensArgsWithType
  | types.SyntheticForwardTransaction
  | types.SyntheticForwardTransactionArgsWithType
  | types.SyntheticWriteData
  | types.SyntheticWriteDataArgsWithType
  | types.SystemGenesis
  | types.SystemGenesisArgsWithType
  | types.SystemWriteData
  | types.SystemWriteDataArgsWithType
  | types.TransferCredits
  | types.TransferCreditsArgsWithType
  | types.UpdateAccountAuth
  | types.UpdateAccountAuthArgsWithType
  | types.UpdateKey
  | types.UpdateKeyArgsWithType
  | types.UpdateKeyPage
  | types.UpdateKeyPageArgsWithType
  | types.WriteData
  | types.WriteDataArgsWithType
  | types.WriteDataTo
  | types.WriteDataToArgsWithType;

/** @ignore */
export namespace TransactionBody {
  export function fromObject(obj: TransactionBodyArgs): TransactionBody {
    if (obj instanceof types.AcmeFaucet) return obj;
    if (obj instanceof types.ActivateProtocolVersion) return obj;
    if (obj instanceof types.AddCredits) return obj;
    if (obj instanceof types.BlockValidatorAnchor) return obj;
    if (obj instanceof types.BurnCredits) return obj;
    if (obj instanceof types.BurnTokens) return obj;
    if (obj instanceof types.CreateDataAccount) return obj;
    if (obj instanceof types.CreateIdentity) return obj;
    if (obj instanceof types.CreateKeyBook) return obj;
    if (obj instanceof types.CreateKeyPage) return obj;
    if (obj instanceof types.CreateLiteTokenAccount) return obj;
    if (obj instanceof types.CreateToken) return obj;
    if (obj instanceof types.CreateTokenAccount) return obj;
    if (obj instanceof types.DirectoryAnchor) return obj;
    if (obj instanceof types.IssueTokens) return obj;
    if (obj instanceof types.LockAccount) return obj;
    if (obj instanceof types.NetworkMaintenance) return obj;
    if (obj instanceof types.RemoteTransaction) return obj;
    if (obj instanceof types.SendTokens) return obj;
    if (obj instanceof types.SyntheticBurnTokens) return obj;
    if (obj instanceof types.SyntheticCreateIdentity) return obj;
    if (obj instanceof types.SyntheticDepositCredits) return obj;
    if (obj instanceof types.SyntheticDepositTokens) return obj;
    if (obj instanceof types.SyntheticForwardTransaction) return obj;
    if (obj instanceof types.SyntheticWriteData) return obj;
    if (obj instanceof types.SystemGenesis) return obj;
    if (obj instanceof types.SystemWriteData) return obj;
    if (obj instanceof types.TransferCredits) return obj;
    if (obj instanceof types.UpdateAccountAuth) return obj;
    if (obj instanceof types.UpdateKey) return obj;
    if (obj instanceof types.UpdateKeyPage) return obj;
    if (obj instanceof types.WriteData) return obj;
    if (obj instanceof types.WriteDataTo) return obj;

    switch (obj.type) {
      case types.TransactionType.AcmeFaucet:
      case "acmeFaucet":
        return new types.AcmeFaucet(obj);
      case types.TransactionType.ActivateProtocolVersion:
      case "activateProtocolVersion":
        return new types.ActivateProtocolVersion(obj);
      case types.TransactionType.AddCredits:
      case "addCredits":
        return new types.AddCredits(obj);
      case types.TransactionType.BlockValidatorAnchor:
      case "blockValidatorAnchor":
        return new types.BlockValidatorAnchor(obj);
      case types.TransactionType.BurnCredits:
      case "burnCredits":
        return new types.BurnCredits(obj);
      case types.TransactionType.BurnTokens:
      case "burnTokens":
        return new types.BurnTokens(obj);
      case types.TransactionType.CreateDataAccount:
      case "createDataAccount":
        return new types.CreateDataAccount(obj);
      case types.TransactionType.CreateIdentity:
      case "createIdentity":
        return new types.CreateIdentity(obj);
      case types.TransactionType.CreateKeyBook:
      case "createKeyBook":
        return new types.CreateKeyBook(obj);
      case types.TransactionType.CreateKeyPage:
      case "createKeyPage":
        return new types.CreateKeyPage(obj);
      case types.TransactionType.CreateLiteTokenAccount:
      case "createLiteTokenAccount":
        return new types.CreateLiteTokenAccount(obj);
      case types.TransactionType.CreateToken:
      case "createToken":
        return new types.CreateToken(obj);
      case types.TransactionType.CreateTokenAccount:
      case "createTokenAccount":
        return new types.CreateTokenAccount(obj);
      case types.TransactionType.DirectoryAnchor:
      case "directoryAnchor":
        return new types.DirectoryAnchor(obj);
      case types.TransactionType.IssueTokens:
      case "issueTokens":
        return new types.IssueTokens(obj);
      case types.TransactionType.LockAccount:
      case "lockAccount":
        return new types.LockAccount(obj);
      case types.TransactionType.NetworkMaintenance:
      case "networkMaintenance":
        return new types.NetworkMaintenance(obj);
      case types.TransactionType.Remote:
      case "remote":
        return new types.RemoteTransaction(obj);
      case types.TransactionType.SendTokens:
      case "sendTokens":
        return new types.SendTokens(obj);
      case types.TransactionType.SyntheticBurnTokens:
      case "syntheticBurnTokens":
        return new types.SyntheticBurnTokens(obj);
      case types.TransactionType.SyntheticCreateIdentity:
      case "syntheticCreateIdentity":
        return new types.SyntheticCreateIdentity(obj);
      case types.TransactionType.SyntheticDepositCredits:
      case "syntheticDepositCredits":
        return new types.SyntheticDepositCredits(obj);
      case types.TransactionType.SyntheticDepositTokens:
      case "syntheticDepositTokens":
        return new types.SyntheticDepositTokens(obj);
      case types.TransactionType.SyntheticForwardTransaction:
      case "syntheticForwardTransaction":
        return new types.SyntheticForwardTransaction(obj);
      case types.TransactionType.SyntheticWriteData:
      case "syntheticWriteData":
        return new types.SyntheticWriteData(obj);
      case types.TransactionType.SystemGenesis:
      case "systemGenesis":
        return new types.SystemGenesis(obj);
      case types.TransactionType.SystemWriteData:
      case "systemWriteData":
        return new types.SystemWriteData(obj);
      case types.TransactionType.TransferCredits:
      case "transferCredits":
        return new types.TransferCredits(obj);
      case types.TransactionType.UpdateAccountAuth:
      case "updateAccountAuth":
        return new types.UpdateAccountAuth(obj);
      case types.TransactionType.UpdateKey:
      case "updateKey":
        return new types.UpdateKey(obj);
      case types.TransactionType.UpdateKeyPage:
      case "updateKeyPage":
        return new types.UpdateKeyPage(obj);
      case types.TransactionType.WriteData:
      case "writeData":
        return new types.WriteData(obj);
      case types.TransactionType.WriteDataTo:
      case "writeDataTo":
        return new types.WriteDataTo(obj);
      default:
        throw new Error(`Unknown transaction body '${(obj as any).type}'`);
    }
  }
}

export type AccountAuthOperation =
  | types.AddAccountAuthorityOperation
  | types.DisableAccountAuthOperation
  | types.EnableAccountAuthOperation
  | types.RemoveAccountAuthorityOperation;

export type AccountAuthOperationArgs =
  | types.AddAccountAuthorityOperation
  | types.AddAccountAuthorityOperationArgsWithType
  | types.DisableAccountAuthOperation
  | types.DisableAccountAuthOperationArgsWithType
  | types.EnableAccountAuthOperation
  | types.EnableAccountAuthOperationArgsWithType
  | types.RemoveAccountAuthorityOperation
  | types.RemoveAccountAuthorityOperationArgsWithType;

/** @ignore */
export namespace AccountAuthOperation {
  export function fromObject(obj: AccountAuthOperationArgs): AccountAuthOperation {
    if (obj instanceof types.AddAccountAuthorityOperation) return obj;
    if (obj instanceof types.DisableAccountAuthOperation) return obj;
    if (obj instanceof types.EnableAccountAuthOperation) return obj;
    if (obj instanceof types.RemoveAccountAuthorityOperation) return obj;

    switch (obj.type) {
      case types.AccountAuthOperationType.AddAuthority:
      case "addAuthority":
        return new types.AddAccountAuthorityOperation(obj);
      case types.AccountAuthOperationType.Disable:
      case "disable":
        return new types.DisableAccountAuthOperation(obj);
      case types.AccountAuthOperationType.Enable:
      case "enable":
        return new types.EnableAccountAuthOperation(obj);
      case types.AccountAuthOperationType.RemoveAuthority:
      case "removeAuthority":
        return new types.RemoveAccountAuthorityOperation(obj);
      default:
        throw new Error(`Unknown account auth operation '${(obj as any).type}'`);
    }
  }
}

export type KeyPageOperation =
  | types.AddKeyOperation
  | types.RemoveKeyOperation
  | types.SetRejectThresholdKeyPageOperation
  | types.SetResponseThresholdKeyPageOperation
  | types.SetThresholdKeyPageOperation
  | types.UpdateAllowedKeyPageOperation
  | types.UpdateKeyOperation;

export type KeyPageOperationArgs =
  | types.AddKeyOperation
  | types.AddKeyOperationArgsWithType
  | types.RemoveKeyOperation
  | types.RemoveKeyOperationArgsWithType
  | types.SetRejectThresholdKeyPageOperation
  | types.SetRejectThresholdKeyPageOperationArgsWithType
  | types.SetResponseThresholdKeyPageOperation
  | types.SetResponseThresholdKeyPageOperationArgsWithType
  | types.SetThresholdKeyPageOperation
  | types.SetThresholdKeyPageOperationArgsWithType
  | types.UpdateAllowedKeyPageOperation
  | types.UpdateAllowedKeyPageOperationArgsWithType
  | types.UpdateKeyOperation
  | types.UpdateKeyOperationArgsWithType;

/** @ignore */
export namespace KeyPageOperation {
  export function fromObject(obj: KeyPageOperationArgs): KeyPageOperation {
    if (obj instanceof types.AddKeyOperation) return obj;
    if (obj instanceof types.RemoveKeyOperation) return obj;
    if (obj instanceof types.SetRejectThresholdKeyPageOperation) return obj;
    if (obj instanceof types.SetResponseThresholdKeyPageOperation) return obj;
    if (obj instanceof types.SetThresholdKeyPageOperation) return obj;
    if (obj instanceof types.UpdateAllowedKeyPageOperation) return obj;
    if (obj instanceof types.UpdateKeyOperation) return obj;

    switch (obj.type) {
      case types.KeyPageOperationType.Add:
      case "add":
        return new types.AddKeyOperation(obj);
      case types.KeyPageOperationType.Remove:
      case "remove":
        return new types.RemoveKeyOperation(obj);
      case types.KeyPageOperationType.SetRejectThreshold:
      case "setRejectThreshold":
        return new types.SetRejectThresholdKeyPageOperation(obj);
      case types.KeyPageOperationType.SetResponseThreshold:
      case "setResponseThreshold":
        return new types.SetResponseThresholdKeyPageOperation(obj);
      case types.KeyPageOperationType.SetThreshold:
      case "setThreshold":
        return new types.SetThresholdKeyPageOperation(obj);
      case types.KeyPageOperationType.UpdateAllowed:
      case "updateAllowed":
        return new types.UpdateAllowedKeyPageOperation(obj);
      case types.KeyPageOperationType.Update:
      case "update":
        return new types.UpdateKeyOperation(obj);
      default:
        throw new Error(`Unknown key page operation '${(obj as any).type}'`);
    }
  }
}

export type Signature =
  | types.AuthoritySignature
  | types.BTCLegacySignature
  | types.BTCSignature
  | types.DelegatedSignature
  | types.ED25519Signature
  | types.ETHSignature
  | types.EcdsaSha256Signature
  | types.InternalSignature
  | types.LegacyED25519Signature
  | types.PartitionSignature
  | types.RCD1Signature
  | types.ReceiptSignature
  | types.RemoteSignature
  | types.RsaSha256Signature
  | types.SignatureSet
  | types.TypedDataSignature;

export type SignatureArgs =
  | types.AuthoritySignature
  | types.AuthoritySignatureArgsWithType
  | types.BTCLegacySignature
  | types.BTCLegacySignatureArgsWithType
  | types.BTCSignature
  | types.BTCSignatureArgsWithType
  | types.DelegatedSignature
  | types.DelegatedSignatureArgsWithType
  | types.ED25519Signature
  | types.ED25519SignatureArgsWithType
  | types.ETHSignature
  | types.ETHSignatureArgsWithType
  | types.EcdsaSha256Signature
  | types.EcdsaSha256SignatureArgsWithType
  | types.InternalSignature
  | types.InternalSignatureArgsWithType
  | types.LegacyED25519Signature
  | types.LegacyED25519SignatureArgsWithType
  | types.PartitionSignature
  | types.PartitionSignatureArgsWithType
  | types.RCD1Signature
  | types.RCD1SignatureArgsWithType
  | types.ReceiptSignature
  | types.ReceiptSignatureArgsWithType
  | types.RemoteSignature
  | types.RemoteSignatureArgsWithType
  | types.RsaSha256Signature
  | types.RsaSha256SignatureArgsWithType
  | types.SignatureSet
  | types.SignatureSetArgsWithType
  | types.TypedDataSignature
  | types.TypedDataSignatureArgsWithType;

/** @ignore */
export namespace Signature {
  export function fromObject(obj: SignatureArgs): Signature {
    if (obj instanceof types.AuthoritySignature) return obj;
    if (obj instanceof types.BTCLegacySignature) return obj;
    if (obj instanceof types.BTCSignature) return obj;
    if (obj instanceof types.DelegatedSignature) return obj;
    if (obj instanceof types.ED25519Signature) return obj;
    if (obj instanceof types.ETHSignature) return obj;
    if (obj instanceof types.EcdsaSha256Signature) return obj;
    if (obj instanceof types.InternalSignature) return obj;
    if (obj instanceof types.LegacyED25519Signature) return obj;
    if (obj instanceof types.PartitionSignature) return obj;
    if (obj instanceof types.RCD1Signature) return obj;
    if (obj instanceof types.ReceiptSignature) return obj;
    if (obj instanceof types.RemoteSignature) return obj;
    if (obj instanceof types.RsaSha256Signature) return obj;
    if (obj instanceof types.SignatureSet) return obj;
    if (obj instanceof types.TypedDataSignature) return obj;

    switch (obj.type) {
      case types.SignatureType.Authority:
      case "authority":
        return new types.AuthoritySignature(obj);
      case types.SignatureType.BTCLegacy:
      case "btclegacy":
        return new types.BTCLegacySignature(obj);
      case types.SignatureType.BTC:
      case "btc":
        return new types.BTCSignature(obj);
      case types.SignatureType.Delegated:
      case "delegated":
        return new types.DelegatedSignature(obj);
      case types.SignatureType.ED25519:
      case "ed25519":
        return new types.ED25519Signature(obj);
      case types.SignatureType.ETH:
      case "eth":
        return new types.ETHSignature(obj);
      case types.SignatureType.EcdsaSha256:
      case "ecdsaSha256":
        return new types.EcdsaSha256Signature(obj);
      case types.SignatureType.Internal:
      case "internal":
        return new types.InternalSignature(obj);
      case types.SignatureType.LegacyED25519:
      case "legacyED25519":
        return new types.LegacyED25519Signature(obj);
      case types.SignatureType.Partition:
      case "partition":
        return new types.PartitionSignature(obj);
      case types.SignatureType.RCD1:
      case "rcd1":
        return new types.RCD1Signature(obj);
      case types.SignatureType.Receipt:
      case "receipt":
        return new types.ReceiptSignature(obj);
      case types.SignatureType.Remote:
      case "remote":
        return new types.RemoteSignature(obj);
      case types.SignatureType.RsaSha256:
      case "rsaSha256":
        return new types.RsaSha256Signature(obj);
      case types.SignatureType.Set:
      case "set":
        return new types.SignatureSet(obj);
      case types.SignatureType.TypedData:
      case "typedData":
        return new types.TypedDataSignature(obj);
      default:
        throw new Error(`Unknown signature '${(obj as any).type}'`);
    }
  }
}

export type NetworkMaintenanceOperation = types.PendingTransactionGCOperation;

export type NetworkMaintenanceOperationArgs =
  | types.PendingTransactionGCOperation
  | types.PendingTransactionGCOperationArgsWithType;

/** @ignore */
export namespace NetworkMaintenanceOperation {
  export function fromObject(obj: NetworkMaintenanceOperationArgs): NetworkMaintenanceOperation {
    if (obj instanceof types.PendingTransactionGCOperation) return obj;

    switch (obj.type) {
      case types.NetworkMaintenanceOperationType.PendingTransactionGC:
      case "pendingTransactionGC":
        return new types.PendingTransactionGCOperation(obj);
      default:
        throw new Error(`Unknown network maintenance operation '${(obj as any).type}'`);
    }
  }
}
