export * from "./address";
export * as api_v2 from "./api_v2";
export * as api_v3 from "./api_v3";
export * as bip44 from "./bip44";
export * as core from "./core";
export * as errors from "./errors";
export * as ledger from "./ledger/deferred"; // SEE BELOW
export * as merkle from "./merkle";
export * as messaging from "./messaging";
export * as network from "./network";
export * from "./signing";

import { Buffer } from "buffer";

// Ensure global Buffer is available
if (typeof window !== "undefined") {
  window.Buffer = window.Buffer || Buffer;
}

// DO NOT IMPORT ledger UNCONDITIONALLY
//
// ./ledger/deferred intentionally A) only imports types and B) only
// conditionally imports the actual code, to avoid pulling in dependencies to
// rxjs and ledger libraries unless it's actually used.
