/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { ChildProcess } from "child_process";
import treeKill from "tree-kill";
import { Client, RpcError } from "../src/api_v2";
import { Buffer } from "../src/common/buffer";
import { constructIssuerProof } from "../src/common/util";
import {
  AccountAuthOperationArgs,
  AccountAuthOperationType,
  ACME_TOKEN_URL,
  CreateKeyPageArgs,
  KeyPageOperationArgs,
  KeyPageOperationType,
  TransactionType,
  WriteDataArgs,
} from "../src/core";
import { ED25519Key, Signer, SignerWithVersion } from "../src/signing";
import { addCredits, randomBuffer, randomLiteIdentity, randomString, startSim } from "./util";

let client = new Client(process.env.ACC_ENDPOINT || "http://127.0.1.1:26660/v2");
let lid: SignerWithVersion;
let identityUrl: string;
let identityKeyPageTxSigner: SignerWithVersion;

let sim: ChildProcess;
beforeAll(
  async () =>
    await startSim((proc, port) => {
      sim = proc;
      client = new Client(`http://127.0.1.1:${port}/v2`);
    }),
);
afterAll(() => sim?.pid && treeKill(sim.pid));

describe("Test Accumulate client", () => {
  beforeAll(async () => {
    /**
     *  Initialize a LiteIdentity with credits
     */
    lid = await randomLiteIdentity();
    const txres = await client.faucet(lid.url.join("ACME"));
    if (
      (txres.transactionHash as any) !==
      "0100000000000000000000000000000000000000000000000000000000000000"
    )
      await client.waitOnTx(txres.txid!.toString());

    // Assert lite identity and lite token account type
    let res = await client.queryUrl(lid.url);
    expect(res.data.type).toStrictEqual("liteIdentity");
    res = await client.queryUrl(lid.url.join("ACME"));
    expect(res.data.type).toStrictEqual("liteTokenAccount");

    await addCredits(client, lid.url, 60_000, lid);

    /**
     *  Initialize an identity
     */
    identityUrl = `acc://${randomString()}.acme`;
    const identitySigner = await ED25519Key.generate();
    const bookUrl = identityUrl + "/my-book";

    // Create identity
    const createIdentity = {
      url: identityUrl,
      keyHash: identitySigner.address.publicKeyHash,
      keyBookUrl: bookUrl,
    };

    res = await client.createIdentity(lid.url, createIdentity, lid);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(identityUrl);
    expect(res.type).toStrictEqual("identity");

    const keyPageUrl = bookUrl + "/1";
    await addCredits(client, keyPageUrl, 600_000, lid);

    identityKeyPageTxSigner = (await Signer.forPage(keyPageUrl, identitySigner)).withVersion(1);
  });

  test("should send tokens", async () => {
    const recipient = (await randomLiteIdentity()).url.join("ACME");

    const amount = 12n;
    const sendTokens = { to: [{ url: recipient, amount: amount }] };
    const { txid } = await client.sendTokens(lid.url.join("ACME"), sendTokens, lid);

    await client.waitOnTx(txid);

    const { data } = await client.queryUrl(recipient);
    expect(BigInt(data.balance)).toStrictEqual(amount);

    let res = await client.queryTx(txid);
    expect(res.type).toStrictEqual("sendTokens");
    expect(res.txid).toStrictEqual(txid);

    // test query with just hash
    res = await client.queryTx(txid.toString().slice(6).split("@")[0]);
    expect(res.type).toStrictEqual("sendTokens");
    expect(res.txid).toStrictEqual(txid);
  });

  test("should burn tokens", async () => {
    let res = await client.queryUrl(lid.url.join("ACME"));
    const originalBalance = BigInt(res.data.balance);

    const amount = 15n;
    const burnTokens = { amount };
    res = await client.burnTokens(lid.url.join("ACME"), burnTokens, lid);

    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(lid.url.join("ACME"));
    expect(BigInt(res.data.balance)).toStrictEqual(originalBalance - amount);
  });

  test("should create an ACME token account", async () => {
    // Create token account
    const tokenAccountUrl = identityUrl + "/ACME";
    const createTokenAccount = {
      url: tokenAccountUrl,
      tokenUrl: ACME_TOKEN_URL,
    };
    const txRes = await client.createTokenAccount(
      identityUrl,
      createTokenAccount,
      identityKeyPageTxSigner,
    );
    await client.waitOnTx(txRes.txid!.toString());

    const res = await client.queryUrl(tokenAccountUrl);
    expect(res.type).toStrictEqual("tokenAccount");
  });

  test("should create key book and manage pages", async () => {
    // Create a new key book
    const page1Signer = await ED25519Key.generate();
    const newKeyBookUrl = identityUrl + "/" + randomString();
    const createKeyBook = {
      url: newKeyBookUrl,
      publicKeyHash: page1Signer.address.publicKeyHash,
    };

    const txRes = await client.createKeyBook(identityUrl, createKeyBook, identityKeyPageTxSigner);
    await client.waitOnTx(txRes.txid!.toString());

    let res = await client.queryUrl(newKeyBookUrl);
    expect(res.type).toStrictEqual("keyBook");

    // verify page is part of the book
    const page1Url = newKeyBookUrl + "/1";
    res = await client.queryUrl(page1Url);
    expect(res.data.keyBook).toStrictEqual(newKeyBookUrl.toString());
    await addCredits(client, page1Url, 20_000, lid);

    let keyPage1TxSigner = (await Signer.forPage(page1Url, page1Signer)).withVersion(1);

    // Add new key to keypage
    const newKey = await ED25519Key.generate();
    const addKeyToPage: KeyPageOperationArgs = {
      type: KeyPageOperationType.Add,
      entry: { keyHash: newKey.address.publicKeyHash },
    };

    res = await client.updateKeyPage(page1Url, addKeyToPage, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(page1Url);
    expect(res.data.keys.length).toStrictEqual(2);

    // Update keyhash in keypage
    let version = await client.querySignerVersion(keyPage1TxSigner);
    keyPage1TxSigner = keyPage1TxSigner.withVersion(version);
    const newNewKey = await ED25519Key.generate();
    const updateKeyPage: KeyPageOperationArgs = {
      type: KeyPageOperationType.Update,
      oldEntry: { keyHash: newKey.address.publicKeyHash },
      newEntry: { keyHash: newNewKey.address.publicKeyHash },
    };
    res = await client.updateKeyPage(page1Url, updateKeyPage, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(page1Url);
    const newKeyHash = Buffer.from(newNewKey.address.publicKeyHash).toString("hex");
    const found = res.data.keys
      .map((k: any) => k.publicKey)
      .find((pk: string) => pk === newKeyHash);
    expect(found).not.toBeUndefined();

    // Set threshold
    // const setThreshold: KeyPageOperation = {
    //   type: KeyPageOperationType.SetThreshold,
    //   threshold: 2,
    // };
    // version = await client.querySignerVersion(keyPage1);
    // keyPage1 = KeypairSigner.withNewVersion(keyPage1, version);
    // let res = await client.updateKeyPage(page1Url, setThreshold, keyPage1);
    // await client.waitOnTx(res.txid!.toString());
    // res = await client.queryUrl(page1Url);
    // expect(res.data.threshold).toStrictEqual(2);

    // Remove key from keypage
    version = await client.querySignerVersion(keyPage1TxSigner);
    keyPage1TxSigner = keyPage1TxSigner.withVersion(version);
    const removeKeyPage: KeyPageOperationArgs = {
      type: KeyPageOperationType.Remove,
      entry: { keyHash: newNewKey.address.publicKeyHash },
    };
    res = await client.updateKeyPage(page1Url, removeKeyPage, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(page1Url);
    expect(res.data.keys.length).toStrictEqual(1);
    expect(res.data.keys[0].publicKey).toStrictEqual(
      Buffer.from(page1Signer.address.publicKeyHash).toString("hex"),
    );

    // Create a new key page to the book
    version = await client.querySignerVersion(keyPage1TxSigner);
    keyPage1TxSigner = keyPage1TxSigner.withVersion(version);
    const page2Signer = await ED25519Key.generate();
    const createKeyPage2: CreateKeyPageArgs = {
      keys: [{ keyHash: page2Signer.address.publicKeyHash }],
    };

    res = await client.createKeyPage(newKeyBookUrl, createKeyPage2, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    const page2Url = newKeyBookUrl + "/2";

    // Update allowed
    const updateAllowed: KeyPageOperationArgs = {
      type: KeyPageOperationType.UpdateAllowed,
      deny: [TransactionType.UpdateKeyPage],
    };

    res = await client.updateKeyPage(page2Url, updateAllowed, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(page2Url);
    expect(res.data.transactionBlacklist).toStrictEqual(["updateKeyPage"]);

    const updateAllowed2: KeyPageOperationArgs = {
      type: KeyPageOperationType.UpdateAllowed,
      allow: [TransactionType.UpdateKeyPage],
    };

    res = await client.updateKeyPage(page2Url, updateAllowed2, keyPage1TxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryUrl(page2Url);
    expect(res.data.transactionBlacklist).toBeUndefined();

    // Test query key page index
    res = await client.queryKeyPageIndex(newKeyBookUrl, page1Signer.address.publicKey);
    expect(res.data.index).toStrictEqual(0);
    res = await client.queryKeyPageIndex(newKeyBookUrl, page2Signer.address.publicKey);
    // TODO
    // expect(res.data.index).toStrictEqual(1);

    // Test query tx history
    res = await client.queryTxHistory(keyPage1TxSigner.url, { start: 0, count: 3 });
    expect(res.type).toStrictEqual("txHistory");
    expect(res.items.length).toStrictEqual(3);
    res = await client.queryTxHistory(keyPage1TxSigner.url, { start: 0, count: res.total });
    expect(res.items.length).toStrictEqual(res.total);
  });

  test("should create data account and write data", async () => {
    // Create data account
    const dataAccountUrl = identityUrl + "/my-data";
    const createDataAccount = {
      url: dataAccountUrl,
    };

    const txRes = await client.createDataAccount(
      identityUrl,
      createDataAccount,
      identityKeyPageTxSigner,
    );
    await client.waitOnTx(txRes.txid!.toString());

    let res = await client.queryUrl(dataAccountUrl);
    expect(res.type).toStrictEqual("dataAccount");

    // Write data
    const data = [Buffer.from("foo"), Buffer.from("bar"), Buffer.from("baz")];
    const writeData: WriteDataArgs = {
      entry: {
        type: "accumulate",
        data: data,
      },
    };

    res = await client.writeData(dataAccountUrl, writeData, identityKeyPageTxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryData(dataAccountUrl);
    expect(res).toBeTruthy();

    res = await client.queryData(dataAccountUrl);
    expect(res.type).toStrictEqual("dataEntry");
    expect(res.data.entry.data[0]).toStrictEqual(data[0].toString("hex"));
    expect(res.data.entry.data[1]).toStrictEqual(data[1].toString("hex"));
    expect(res.data.entry.data[2]).toStrictEqual(data[2].toString("hex"));
    expect(res.data.entry.data.length).toStrictEqual(3);
    const firstEntryHash = res.data.entryHash;

    const data2 = [randomBuffer(), randomBuffer(), randomBuffer(), randomBuffer(), randomBuffer()];
    const writeData2: WriteDataArgs = {
      entry: {
        type: "accumulate",
        data: data2,
      },
    };
    res = await client.writeData(dataAccountUrl, writeData2, identityKeyPageTxSigner);
    await client.waitOnTx(res.txid!.toString());

    res = await client.queryDataSet(dataAccountUrl, { start: 0, count: 10 });
    expect(res.items.length).toStrictEqual(2);
    expect(res.total).toStrictEqual(2);

    // Query Data should now return the latest entry
    res = await client.queryData(dataAccountUrl);
    expect(res.data.entry.data[0]).toStrictEqual(data2[0].toString("hex"));
    expect(res.data.entry.data.length).toStrictEqual(5);
    // Query data per entry hash
    res = await client.queryData(dataAccountUrl, firstEntryHash);
    expect(res.data.entry.data[0]).toStrictEqual(data[0].toString("hex"));
  });

  test("should create token and token account for it", async () => {
    const tokenUrl = identityUrl + "/TEST";
    const createToken = {
      url: tokenUrl,
      symbol: "TEST",
      precision: 0,
    };

    let txRes = await client.createToken(identityUrl, createToken, identityKeyPageTxSigner);
    const createTokenTxId = txRes.txid;
    await client.waitOnTx(createTokenTxId);

    const recipient = (await Signer.forLite(await ED25519Key.generate())).url.join(tokenUrl);
    const amount = 123n;
    const issueToken = {
      to: [{ url: recipient, amount }],
    };

    txRes = await client.issueTokens(tokenUrl, issueToken, identityKeyPageTxSigner);
    await client.waitOnTx(txRes.txid!.toString());

    const { data } = await client.queryUrl(recipient);
    expect(BigInt(data.balance)).toStrictEqual(amount);

    // Create a token account for the TEST token
    const tokenAccountUrl = identityUrl + "/TEST2";
    const createTokenAccount = {
      url: tokenAccountUrl,
      tokenUrl,
      proof: await constructIssuerProof(client, tokenUrl),
    };
    txRes = await client.createTokenAccount(
      identityUrl,
      createTokenAccount,
      identityKeyPageTxSigner,
    );

    await client.waitOnTx(txRes.txid, { timeout: 10_000 });

    const res = await client.queryUrl(tokenAccountUrl);
    expect(res.type).toStrictEqual("tokenAccount");
  });

  test("should update keypage key", async () => {
    const newKey = await ED25519Key.generate();
    const updateKey = {
      newKeyHash: newKey.address.publicKeyHash,
    };

    const txRes = await client.updateKey(
      identityKeyPageTxSigner.url,
      updateKey,
      identityKeyPageTxSigner,
    );
    await client.waitOnTx(txRes.txid!.toString());

    const res = await client.queryUrl(identityKeyPageTxSigner.url);
    expect(res.data.keys[0].publicKeyHash).toStrictEqual(
      Buffer.from(newKey.address.publicKeyHash).toString("hex"),
    );
  });

  xtest("should update account auth", async () => {
    // Disable
    const disable: AccountAuthOperationArgs = {
      type: AccountAuthOperationType.Disable,
      authority: identityKeyPageTxSigner.url,
    };

    const res = await client.updateAccountAuth(
      identityKeyPageTxSigner.url,
      disable,
      identityKeyPageTxSigner,
    );
    await client.waitOnTx(res.txid!.toString());

    // // Enable
    // const enable: AccountAuthOperation = {
    //   type: AccountAuthOperationType.Enable,
    //   authority: identityKeyPageTxSigner.url,
    // };

    // res = await client.updateAccountAuth(
    //   identityKeyPageTxSigner.url,
    //   enable,
    //   identityKeyPageTxSigner
    // );
    // await waitOn(async () => client.queryTx(res.txid));

    // // Add authority
    // const addAuthority: AccountAuthOperation = {
    //   type: AccountAuthOperationType.AddAuthority,
    //   authority: "xxxxxx",
    // };

    // res = await client.updateAccountAuth(
    //   identityKeyPageTxSigner.url,
    //   addAuthority,
    //   identityKeyPageTxSigner
    // );
    // await waitOn(async () => client.queryTx(res.txid));

    // // Remove authority
    // const removeAuthority: AccountAuthOperation = {
    //   type: AccountAuthOperationType.RemoveAuthority,
    //   authority: "xxxxxx",
    // };

    // res = await client.updateAccountAuth(
    //   identityKeyPageTxSigner.url,
    //   removeAuthority,
    //   identityKeyPageTxSigner
    // );
    // await waitOn(async () => client.queryTx(res.txid));
  });

  test("should query directory", async () => {
    // This test result depends on execution of other identity tests
    // and should be positioned after those
    let res = await client.queryDirectory(identityUrl, { start: 0, count: 3 });
    expect(res.type).toStrictEqual("directory");
    expect(res.items.length).toStrictEqual(3);
    res = await client.queryDirectory(identityUrl, { start: 0, count: res.total });
    expect(res.items.length).toStrictEqual(res.total);
  });

  test("should get version", async () => {
    const res = await client.version();
    expect(res.type).toStrictEqual("version");
  });

  test("should call describe", async () => {
    const res = await client.describe();
    expect(res).toBeTruthy();
  });

  test("should call status", async () => {
    const res = await client.status();
    expect(res).toBeTruthy();
  });

  xtest("should get metrics", async () => {
    const res = await client.metrics("tps", 60);
    expect(res.type).toStrictEqual("metrics");
  });

  test("should query major blocks", async () => {
    const res = await client.queryMajorBlocks(identityUrl, { count: 1, start: 0 });
    expect(res).toBeTruthy();
  });

  test("should query minor blocks", async () => {
    const res = await client.queryMinorBlocks(
      identityUrl,
      { count: 1, start: 0 },
      {
        txFetchMode: "ids",
        blockFilterMode: "excludenone",
      },
    );
    expect(res).toBeTruthy();
  });

  test("should reject unknown method", async () => {
    try {
      await client.call("unknown");
    } catch (e: any) {
      expect(e).toBeInstanceOf(RpcError);
      return;
    }
    throw "should have thrown";
  });
});
